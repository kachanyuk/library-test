#!/usr/bin/env python
# -*- coding: utf-8 -*-


def import_oauth_class_old(m):
    print "<import_oauth_class> m", m
    m = m.split('.')
    c = m.pop(-1)
    module = __import__('.'.join(m), fromlist=[c])
    print "<import_oauth_class>"
    return getattr(module, c)


def import_oauth_class(full_path):
    path_split = full_path.split('.')
    path = ".".join(path_split[:-1])
    klass = path_split[-1:]
    mod = __import__(path, fromlist=[klass])
    print "<import_oauth_class> ,full_path, path_split , path, klass : ",full_path, path_split , path, klass
    return getattr(mod, klass[0])

