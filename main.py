#!/usr/bin/env python
# encoding: utf-8


import sys
import os
import webapp2
from webapp2 import Route
from jinja2.filters import do_pprint
from src.model.index import *
from src.auth import auth

sys.path[0:0] = ['lib', 'src', 'src/emotions', 'lib/socialoauth/sites', 'lib/socialoauth']

DEBUG = os.environ.get('SERVER_SOFTWARE', '').startswith('Dev')

config = {
    'webapp2_extras.sessions': {'secret_key': 'wIDjEesObzp5nonpRHDzSp40aba7STuqC6ZRY'},
    'webapp2_extras.auth': {'user_attributes': ['displayName', 'email'], },
    'webapp2_extras.jinja2': {'filters': {'do_pprint': do_pprint, }, },
}

routes = [
    Route(r'/', handler='handlers.Index:root', name='main'),
    Route(r'/Install', Install, name='Install'),
    Route('/login', handler='src.auth.login', name='login'),
    Route('/account/oauth/<sitename>', handler='auth.callback', name='callback'),
    Route('/logout', handler='auth.logout', name='logout'),
    Route('/oautherror', handler='auth.oautherror', name='oautherror'),
    Route('/api/<model>', Api, name='api'),
    Route('/api/<model>/<model_id>', Api, name='api'),
    Route('/upload', UploadHandler, name='UploadHandler'),
    Route('/serve/<blob_key>', ServeHandler, name='ServeHandler')
        ]

application = webapp2.WSGIApplication(routes, debug=True, config=config)


