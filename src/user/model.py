#!/usr/bin/env python
# -*- coding: utf-8 -*-

###################################################################################################
#
#  @author Yuri Kachanyuk
#  @author mail: wku@ukr.net, yurikachanyuk@gmail.com; skape : wku-lg; https://vk.com/kachanyuk
#
###################################################################################################


from google.appengine.ext import ndb
from webapp2_extras import securecookie
from webapp2_extras import security

class Users(ndb.Model):
    site_name = ndb.StringProperty()
    uid = ndb.StringProperty()
    sessionId = ndb.StringProperty()
    avatar = ndb.StringProperty()
    name = ndb.StringProperty()
    accessGroup = ndb.StringProperty(default="")

class Sessions(ndb.Model):
    sessionId = ndb.StringProperty()
    uid = ndb.StringProperty()
    name = ndb.StringProperty()







