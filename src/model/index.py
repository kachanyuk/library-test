#!/usr/bin/env python
# -*- coding: utf-8 -*-

###################################################################################################
#
#  @author Yuri Kachanyuk
#  @author mail: wku@ukr.net, yurikachanyuk@gmail.com; skape : wku-lg; https://vk.com/kachanyuk
#
###################################################################################################


from unittest import result
import urllib2
import urllib
import json
import sys
import datetime
import webapp2
from google.appengine.ext import ndb
import src.config
import datetime
import json
import datetime
from time import mktime
from src.model.model import *

sys.path[0:0] = ['lib', 'src']
sys.modules['ndb'] = ndb


class JSONDateTimeEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, (datetime.date, datetime.datetime)):
            return obj.isoformat()
        else:
            return json.JSONEncoder.default(self, obj)

class Api(webapp2.RequestHandler):
    def post(self, model, model_id=None):
        self.request.path_info_pop()
        dictionary = json.loads(self.request.body)
        if not model_id:
            newObject = globals()[model](**dictionary)
            result = dictionary
            id = str( newObject.put().id() )
            result.update( {'id': id} )
            self.response.write(json.dumps([result] ))
        else:
            item = globals()[model].get_by_id(int(model_id))
            for key, value in dictionary.items():
                setattr(item, key, value)
            item.put()


    def get(self, model, model_id = None):
        self.request.path_info_pop()
        if not model_id:
            everyItem = []
            dictionary = dict( self.request.GET.items())
            if dictionary.get('uid'):
                value = globals()[model].query(globals()[model].uid==dictionary.get('uid') )
            else:
                value = globals()[model].query()
            for item in value:
                item_dict = item.to_dict()
                item_dict['id'] = item.key.id()
                everyItem.append(item_dict)
            self.response.write(json.dumps(everyItem, cls=JSONDateTimeEncoder))
        else:
            item = globals()[model].get_by_id(int(model_id))
            if item:
                item_dict = item.to_dict()
                item_dict['id'] = item.key.id()
                self.response.write(json.dumps(item_dict, cls=JSONDateTimeEncoder))
            else:
                self.response.write( [] )


    def get1(self, model, model_id = None):
        self.request.path_info_pop()
        if not model_id:
            everyItem = []
            for item in globals()[model].query():
                item_dict = item.to_dict()
                item_dict['id'] = item.key.id()
                everyItem.append(item_dict)
            self.response.write(json.dumps(everyItem, cls=JSONDateTimeEncoder))
        else:
            item = globals()[model].get_by_id(int(model_id))
            if item:
                item_dict = item.to_dict()
                item_dict['id'] = item.key.id()
                self.response.write(json.dumps(item_dict, cls=JSONDateTimeEncoder))
            else:
                self.response.write([])

    def get_(self, model, model_id = None):
        self.request.path_info_pop()
        if not model_id:
            everyItem = []
            dictionary = dict( self.request.GET.items())
            if dictionary.get('uid'):
                value = globals()[model].query(globals()[model].uid==dictionary.get('uid') )
            else:
                value = globals()[model].query()
            for item in value:
                item_dict = item.to_dict()
                item_dict['id'] = item.key.id()
                everyItem.append(item_dict)
            self.response.write(json.dumps(everyItem, cls=JSONDateTimeEncoder))
        else:
            item = globals()[model].get_by_id(int(model_id))
            if item:
                item_dict = item.to_dict()
                item_dict['id'] = item.key.id()
                self.response.write([json.dumps(item_dict, cls=JSONDateTimeEncoder)])
            else:
                self.response.write( [] )

    def delete(self, model, model_id = None):
        self.request.path_info_pop()
        ndb.Key(model, int(model_id)).delete()

