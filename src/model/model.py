#!/usr/bin/env python
# encoding: utf-8

###################################################################################################
#
#  @author Yuri Kachanyuk
#  @author mail: wku@ukr.net, yurikachanyuk@gmail.com; skape : wku-lg; https://vk.com/kachanyuk
#
###################################################################################################


from src.handlers import *

class Library(ndb.Model):
    name = ndb.StringProperty(required=True)
    file = ndb.TextProperty()
    autor = ndb.StringProperty(required=True)
    description = ndb.StringProperty(required=True)
    apiName = ndb.StringProperty(required=True)
    className = ndb.StringProperty(required=True)



def wipe_datastore():
    lessonitemg = Library.query().fetch()
    for t in [lessonitemg]:
        try:
            for i in t:
                i.key.delete()
        except :
            print "Error db ", t

#/Install
class Install(BaseHandlerNew):
    def get (self):
        wipe_datastore()
        from src.model.insert import data
        for i in data["Library"]:
            p = globals()["Library"](
                            name=i["name"],
                            file=i["file"],
                            autor=i["autor"],
                            description=i["description"],
                            apiName=i["apiName"],
                            className=i["className"]
            )
            k = p.put() #.order(Comment.date) #.order(-Comment.date)
        self.response.write( "ok" )




