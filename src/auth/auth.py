#!/usr/bin/env python
# -*- coding: utf-8 -*-

###################################################################################################
#
#  @author Yuri Kachanyuk
#  @author mail: wku@ukr.net, yurikachanyuk@gmail.com; skape : wku-lg; https://vk.com/kachanyuk
#
###################################################################################################


import sys
import os
import json
from src.handlers import  BaseHandlerNew
from lib.socialoauth import SocialSites, SocialAPIError
from helper import Session, UserStorage, gen_session_id
from pprint import pformat
from src.config import *
from src.user.model import Users, Sessions


class login(BaseHandlerNew):

    def get(self):
        session_id = self.session.get('session_id')
        if session_id:
            q = Users.query(Users.sessionId == session_id).fetch(1000)
            if len(q) > 0:
                self.redirect('/')

        def _link(site_class):
            _s = socialsites.get_site_object_by_class(site_class)
            a_content = _s.site_name_zh
            return [_s.authorize_url, _s.site_name_zh, _s.site_name]
        socialsites = SocialSites(SOCIALOAUTH_SITES)
        template_values = map(_link, socialsites.list_sites_class())
        self.render_template('login.html', {'providers':template_values[0]})



class callback(BaseHandlerNew):
    def get(self, sitename):
        code = self.post_get['code']
        if not code:
            self.redirect('/oautherror')
        socialsites = SocialSites(SOCIALOAUTH_SITES)
        s = socialsites.get_site_object_by_name(sitename)
        try:
            s.get_access_token(code)
        except SocialAPIError as e:
            print e.site_name      # Какие сайты OAuth2 ошибку?
            print e.url            # Запрос url
            print e.error_msg      # Сообщение об ошибке возвращаемое сайт / urllib2 Сообщение об ошибке
            raise

        session_id = self.session.get('session_id') #self.session['session_id']

        if not session_id:
            session_id = gen_session_id()
            self.session['session_id'] = session_id

        q = Users.query(Users.uid == s.uid)
        if len(q.fetch(1)) > 0:
            user = q.fetch(1)[0]
            user.sessionId = session_id
            user.put()
        else:
            user = Users(site_name=s.site_name, uid=s.uid, name=s.name, avatar=s.avatar, sessionId=session_id)
            user.put()

        self.redirect('/')


class logout(BaseHandlerNew):
    def get(self):
        session_id = self.session.get('session_id') #self.session['session_id']
        if not session_id:
            self.redirect('/')
        else:
            del (self.session['session_id'])
            self.redirect('/')


class oautherror(BaseHandlerNew):
    def get(self):
        self.redirect('/')



