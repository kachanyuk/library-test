#!/usr/bin/env python
# -*- coding: utf-8 -*-

###################################################################################################
#
#  @author Yuri Kachanyuk
#  @author mail: wku@ukr.net, yurikachanyuk@gmail.com; skape : wku-lg; https://vk.com/kachanyuk
#
###################################################################################################


import wsgiref
import sys
import json
import re
import os
import urllib
import jinja2
import webapp2
from webapp2_extras import sessions, jinja2
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.api import images
from google.appengine.ext import ndb, webapp
from google.appengine.ext.webapp import blobstore_handlers
from google.appengine.ext import blobstore
from google.appengine.api import files
from src.user.model import Users
import config

sys.path[0:0] = ['lib', 'src', 'src/emotions']
sys.modules['ndb'] = ndb

class BaseHandlerNew(webapp2.RequestHandler):
    def dispatch(self):
        self.session_store = sessions.get_store(request=self.request)
        try:
            webapp2.RequestHandler.dispatch(self)
        finally:
            self.session_store.save_sessions(self.response)

    @webapp2.cached_property
    def session(self):
        return self.session_store.get_session()

    @webapp2.cached_property
    def post_get(self):
        a = {}
        try:
            for key, value in self.request.GET.items():
                a[key] = value
        except:
            pass
        try:
            for key, value in self.request.POST.items():
                a[key] = value
        except:
            pass
        if (a):
            self.post_get = a
        else:
            self.post_get = None
        return self.post_get

    @webapp2.cached_property
    def jinja2(self):
        return jinja2.get_jinja2(app=self.app)

    def render_template(self, template_name, template_values={}):
        self.response.write(self.jinja2.render_template(template_name, **template_values))

    def render_string(self, template_string, template_values={}):
        self.response.write(self.jinja2.environment.from_string(template_string).render(**template_values))

    def json_response(self, json):
        self.response.headers.add_header('content-type', 'application/json', charset='utf-8')
        self.response.out.write(json)

    def render_json(self, response):
        self.response.write('%s(%s);' % (self.request.GET['callback'], json.dumps(response)))

class Index(BaseHandlerNew):
    def root(self):
        upload_url = blobstore.create_upload_url('/upload')
        self.session_id = self.session.get('session_id')
        if self.session_id:
            q = Users.query(Users.sessionId == self.session_id).fetch(1000)
            if len(q) > 0:
                user = q[0]
                app_url = wsgiref.util.application_uri(self.request.environ)
                app_url = app_url[0:len(app_url) - 1]
                locale = "en"
                unikey = self.session_id
                template_values = {"CLIENT_USER_AVATAR": user.avatar, "CLIENT_USER_NAME": user.name,
                                   "ID": user.key.id(), "CLIENT_USER_ID": user.uid, "app_url": app_url,
                                   "locale": locale, "token": 1, "wall": 1, "upload_url": upload_url}
                self.render_template('index.html', template_values)
            else:
                template_values = {'upload_url':'', 'ID': ''}
                self.render_template('index.html', template_values)
                #self.redirect("/login")
        else:
            template_values = {'upload_url':'', 'ID': ''}
            self.render_template('index.html', template_values)
            #self.redirect("/login")


class UploadHandler(blobstore_handlers.BlobstoreUploadHandler):
    def post(self):
        upload_files = self.get_uploads('file')
        blob_info = upload_files[0]
        self.response.out.write("/serve/" + str(blob_info.key()))

    def get(self):
        print self.request.body


class ServeHandler(blobstore_handlers.BlobstoreDownloadHandler):
    def get(self, blob_key):
        blob_key = str(urllib.unquote(blob_key))
        if not blobstore.get(blob_key):
            self.error(404)
        else:
            self.send_blob(blobstore.BlobInfo.get(blob_key), save_as=True)


