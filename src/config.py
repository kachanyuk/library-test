#!/usr/bin/env python
# -*- coding: utf-8 -*-

###################################################################################################
#
#  @author Yuri Kachanyuk
#  @author mail: wku@ukr.net, yurikachanyuk@gmail.com; skape : wku-lg; https://vk.com/kachanyuk
#
###################################################################################################


import os

ON_DEV = os.environ.get('SERVER_SOFTWARE', '').startswith('Dev')
print "ON_DEV:",ON_DEV

SOCIALOAUTH_SITES_SERVER = (
        ('facebook', 'lib.socialoauth.sites.facebook.Facebook', 'Facebook',     {
         'redirect_uri': 'http://library-test.appspot.com/account/oauth/facebook',
         'client_id': '129110677296503',
         'client_secret': 'b35628ed67f059c0cf644b2d76a71396',}),)

SOCIALOAUTH_SITES_LOCAL = (
        ('facebook', 'lib.socialoauth.sites.facebook.Facebook', 'Facebook',      {
         'redirect_uri': 'http://localhost:8080/account/oauth/facebook',
         'client_id': '407240376071911',
         'client_secret': 'e360483dc24ae77fefdcd8c00602cbd9', } ),)

if ON_DEV :
    SOCIALOAUTH_SITES = SOCIALOAUTH_SITES_LOCAL
else:
    SOCIALOAUTH_SITES = SOCIALOAUTH_SITES_SERVER
