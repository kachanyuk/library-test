//********************************************************************************
angular.module('Project', ['datastore']).config(function ($routeProvider) {
        $routeProvider.
            when('/', {controller: ListCtrl, templateUrl: '/static/list.html'}).
            when('/edit/:projectId', {controller: EditCtrl, templateUrl: '/static/detail.html'}).
            when('/new', {controller: CreateCtrl, templateUrl: '/static/detail.html'}).
            otherwise({redirectTo: '/'});
});
//********************************************************************************
var myModule = angular.module('datastore', ['ngResource']);
myModule.factory('Apy', function($resource) {
        var Apy = $resource('/:apiName/:className/:id', { className: '@className', id:'@id', apiName:'@apiName' }, { update: { method: 'POST', isArray: true } } );
        Apy.prototype.update = function(cb) { return Apy.update({id: this.id}, angular.extend( {}, this, {_id:undefined}), cb);  };
        Apy.prototype.destroy = function(cb) {return Apy.remove({id: this.id}, cb); };
        return Apy;
});

//********************************************************************************
function ListCtrl($scope, Apy) {
    //console.log("ListCtrl");
    setTimeout(function () {
        $scope.Library = Apy.query({   apiName: "api", className: "Library" }, function () {
            console.log("ID: ", ID)
            $scope.lock = true;;
            if (ID.length>0) $scope.lock = false;
        });
    }, 1500);
}
//********************************************************************************
function CreateCtrl($scope, $location, Apy) {
    //console.log("CreateCtrl: ");
    $scope.upload_url = upload_url;
    $scope.save = function () {
        var fileInput = document.getElementById('idForm');
        var file = fileInput.files[0];
        var formData = new FormData();
        formData.append('file', file);
        var xhr = new XMLHttpRequest();
        xhr.open('POST', upload_url, true);
        xhr.onload = function (oEvent) {
            if (xhr.status == 200) {
                //console.log("oEvent: ", oEvent);
                //console.log("oEvent: ", oEvent.target.response);
                Apy.update({   apiName: "api", className: "Library", id: $scope.Library.id, name: $scope.Library.name, autor: $scope.Library.autor, description: $scope.Library.description, file: oEvent.target.response }, function () {
                    setTimeout(function () {
                    }, 1500);
                    $location.path('/');
                });
            } else {
                //console.log("Error: ", xhr.status);
            }
        };
        xhr.send(formData);
    };
}

//********************************************************************************
function EditCtrl($scope, $location, $routeParams, Apy) {
    //console.log("<EditCtrl>: $routeParams $location", $routeParams, $location);
    projectId = $routeParams.projectId
    tt = $location;
    $scope.Library = Apy.get({ apiName: "api", className: "Library", id: $routeParams.projectId}, function () {
        //console.log("$scope.Library: ", $scope.Library)
    });
    $scope.isClean = function () {
        return angular.equals({}, $scope.Library);
    };
    $scope.destroy = function () {
        $scope.Library = Apy.remove({   apiName: "api", className: "Library", id: $scope.Library.id }, function () {
            $location.path('/list');
        });
    };
    $scope.save = function () {

    //console.log("save: ");
    $scope.upload_url = upload_url;
    //console.log("upload_url: ",upload_url);

    var fileInput = document.getElementById('idForm');
    var file = fileInput.files[0];
    if (!file){
        //console.log("старая картинка");
        Apy.update({   apiName: "api", className: "Library", id: $scope.Library.id, name: $scope.Library.name, autor: $scope.Library.autor, description: $scope.Library.description, file: $scope.Library.file }, function () {
            $location.path('/');
        });
        return;
    }
    //console.log("file: ",file);
    var formData = new FormData();
    formData.append('file', file);
    var xhr = new XMLHttpRequest();
    xhr.open('POST', upload_url, true);
    xhr.onload = function (oEvent) {
        if (xhr.status == 200) {
             //console.log("oEvent: ", oEvent);
             //console.log("oEvent: ", oEvent.target.response);
             Apy.update({   apiName: "api", className: "Library", id: $scope.Library.id, name: $scope.Library.name, autor: $scope.Library.autor, description: $scope.Library.description, file: oEvent.target.response }, function () {
                 //$location.path('/');
        });
        } else {
            //console.log("Error: ", xhr.status);
            if (xhr.status == "404") {
                //window.location.assign('/')
            }
        }
    };

    xhr.send(formData);

    };
}

//********************************************************************************
function readURL(input) {
    //console.log("readURL: ",input);
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#imgInp').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

//********************************************************************************
$("#idForm").change(function () {
    //console.log("change");
    readURL(this);
});

